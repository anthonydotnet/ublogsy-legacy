﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uBlogsy.BusinessLogic;
using uBlogsy.BusinessLogic.Models;
using uHelpsy.Core;
using umbraco.cms.businesslogic.web;
using uBlogsy.BusinessLogic.Extensions;

namespace uBlogsy.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class DateFolderTest
    {
        public DateFolderTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        protected TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion



        #region TestUseAutoSorting

        /// <summary>
        /// Tests getting of "use auto sorting"
        /// </summary>
        [TestMethod]
        public void TestUseAutoSorting()
        {
            // get root
            var landing = Document.GetRootDocuments()[0];

            // set autosort to false
            landing.getProperty("uBlogsyGeneralUseAutoSorting").Value = "0";

            var doc = landing.GetDescendants().Cast<Document>().Where(x => x.ContentType.Alias == "uBlogsyPost").First();
            bool result = DateFolderService.Instance.UseAutoSorting(doc);
            Assert.IsFalse(result, "TestUseAutoSorting");

            // set autosort to true
            landing.getProperty("uBlogsyGeneralUseAutoSorting").Value = "1";
            bool result2 = DateFolderService.Instance.UseAutoSorting(doc);
            Assert.IsTrue(result2, "TestUseAutoSorting");
        }

        #endregion









        #region TestEnsureCorrectParentForPost
        /// <summary>
        /// Tests moving of node to correct parent according to date.
        /// Differs to TestGetCorrectParentForPost() because we are getting the parent AFTER the post is moved.
        /// </summary>
        [TestMethod]
        public void TestEnsureCorrectParentForPost()
        {
            var post = PostServiceTest.CreatePost();

            // test post has correct parent
            post = DateFolderService.Instance.EnsureCorrectParentForPost(post);

            Document parent = new Document(post.ParentId);

            // assert that parent is a day folder
            Assert.AreEqual(parent.ContentType.Alias, "uBlogsyFolderDay");



            // test day folder creation and move
            DateTime minusDay = DateTime.UtcNow.AddDays(-1);
            post.getProperty("uBlogsyPostDate").Value = minusDay;

            Document oldParent = new Document(post.ParentId);
            DateFolderService.Instance.EnsureCorrectParentForPost(post);

            // re-get post
            post = new Document(post.Id);
            parent = new Document(post.ParentId);
            Assert.AreEqual(parent.Text, minusDay.Day.ToString());
            Assert.AreNotEqual(oldParent.Id, parent.Id);
            //------



            // test day folder move back
            DateTime thisDay = DateTime.UtcNow;
            post.getProperty("uBlogsyPostDate").Value = thisDay;
            oldParent = new Document(post.ParentId);
            DateFolderService.Instance.EnsureCorrectParentForPost(post);

            // re-get post
            post = new Document(post.Id);
            parent = new Document(post.ParentId);
            Assert.AreEqual(parent.Text, thisDay.Day.ToString());
            Assert.AreNotEqual(oldParent.Id, parent.Id);
            //------


            // test month folder creation and move
            DateTime minusMonth = DateTime.UtcNow.AddMonths(-1);
            post.getProperty("uBlogsyPostDate").Value = minusMonth;
            oldParent = new Document(post.ParentId);
            DateFolderService.Instance.EnsureCorrectParentForPost(post);

            // re-get post
            post = new Document(post.Id);
            parent = new Document(post.ParentId);
            Assert.AreEqual(parent.Text, minusMonth.Day.ToString());
            Assert.AreNotEqual(oldParent.Id, parent.Id);
            //------

            // test month folder move back
            DateTime thisMonth = DateTime.UtcNow;
            post.getProperty("uBlogsyPostDate").Value = thisMonth;
            oldParent = new Document(post.ParentId);
            DateFolderService.Instance.EnsureCorrectParentForPost(post);

            // re-get post
            post = new Document(post.Id);
            parent = new Document(post.ParentId);
            Assert.AreEqual(parent.Text, thisMonth.Day.ToString());
            Assert.AreNotEqual(oldParent.Id, parent.Id);
            //------




            // test year folder creation and move
            DateTime minusYear = DateTime.UtcNow.AddYears(-1);
            post.getProperty("uBlogsyPostDate").Value = minusYear;
            oldParent = new Document(post.ParentId);
            DateFolderService.Instance.EnsureCorrectParentForPost(post);

            // re-get post
            post = new Document(post.Id);
            parent = new Document(post.ParentId);
            Assert.AreEqual(parent.Text, minusYear.Day.ToString());
            Assert.AreNotEqual(oldParent.Id, parent.Id);
            //------


            // now move node back to existing
            // test year folder move back
            DateTime thisYear = DateTime.UtcNow;
            post.getProperty("uBlogsyPostDate").Value = thisYear;
            oldParent = new Document(post.ParentId);
            DateFolderService.Instance.EnsureCorrectParentForPost(post);

            // re-get post
            post = new Document(post.Id);
            parent = new Document(post.ParentId);
            Assert.AreEqual(parent.Text, thisYear.Day.ToString());
            Assert.AreNotEqual(oldParent.Id, parent.Id);
            //------


            // cleanup
            // clean up
            new Document(oldParent.Parent.ParentId).delete();
            new Document(post.Parent.Parent.ParentId).delete();
        }


        #endregion




        #region TestGetCorrectParentForPost

        /// <summary>
        /// Tests moving of node to correct parent according to date.
        /// Differs to TestEnsureCorrectParentForPost() because we are testing a "get" of the new parent BEFORE the post is moved.
        /// </summary>
        [TestMethod]
        public void TestGetCorrectParentForPost()
        {
            // create a post for today
            var post = PostServiceTest.CreatePost();

            Inherited.DateFolderService dfs = new Inherited.DateFolderService();

            List<Document> docsToDelete = new List<Document>();

            // move the post to correct parent - involes creation of parents
            Document newParent = dfs.TestGetCorrectParentForPost(post);
            post = new Document(post.Id);

            // change date to yesterday
            DateTime minusDay = DateTime.UtcNow.AddDays(-1);
            post.getProperty("uBlogsyPostDate").Value = minusDay;

            Document oldParent = new Document(post.ParentId);

            // move post to yesterday
            newParent = dfs.TestGetCorrectParentForPost(post);

            // test new parent name
            Assert.AreEqual(newParent.Text, minusDay.Day.ToString());

            // assert that new parent is not same as old parent
            Assert.AreNotEqual(newParent.Id, oldParent.Id);


            //------

            // test day folder move back
            DateTime thisDay = DateTime.UtcNow;
            post.getProperty("uBlogsyPostDate").Value = thisDay;

            oldParent = newParent;

            // move post back to today
            newParent = dfs.TestGetCorrectParentForPost(post);
            
           
            // test new parent name
            Assert.AreEqual(newParent.Text, thisDay.Day.ToString());

            // assert that new parent is not same as old parent
            Assert.AreNotEqual(newParent.Id, oldParent.Id);

            // clean up 
            //oldParent.delete();


            //------


            // test month folder creation and move
            DateTime minusMonth = DateTime.UtcNow.AddMonths(-1);
            post.getProperty("uBlogsyPostDate").Value = minusMonth;

            oldParent = newParent;

            // move post to last month
            newParent = dfs.TestGetCorrectParentForPost(post);
            

            // test new parent name
            Assert.AreEqual(newParent.Text, minusMonth.Day.ToString());

            // assert that new parent is not same as old parent
            Assert.AreNotEqual(newParent.Id, oldParent.Id);
            //------


            // test month folder move back
            DateTime thisMonth = DateTime.UtcNow;
            post.getProperty("uBlogsyPostDate").Value = thisMonth;

            oldParent = newParent;

            // move post back to current month
            newParent = dfs.TestGetCorrectParentForPost(post);
            
            // test new parent name
            Assert.AreEqual(newParent.Text, thisMonth.Day.ToString());

            // assert that new parent is not same as old parent
            Assert.AreNotEqual(newParent.Id, oldParent.Id);

            // clean up 
            //oldParent.delete();

            //------



            // test year folder creation and move
            DateTime minusYear = DateTime.UtcNow.AddYears(-1);
            post.getProperty("uBlogsyPostDate").Value = minusYear;

            oldParent = newParent;

            // move post to last year
            newParent = dfs.TestGetCorrectParentForPost(post);

            // test new parent name
            Assert.AreEqual(newParent.Text, minusYear.Day.ToString());

            // assert that new parent is not same as old parent
            Assert.AreNotEqual(newParent.Id, oldParent.Id);
            //------


            // now move node back to existing year
            // test year folder move back
            DateTime thisYear = DateTime.UtcNow;
            post.getProperty("uBlogsyPostDate").Value = thisYear;

            oldParent = newParent;

            // move post back to this year
            newParent = dfs.TestGetCorrectParentForPost(post);

            // test new parent name
            Assert.AreEqual(newParent.Text, thisYear.Day.ToString());

            // assert that new parent is not same as old parent
            Assert.AreNotEqual(newParent.Id, oldParent.Id);

            // clean up 
            //oldParent.delete();


            //------


            // clean up
            new Document(oldParent.Parent.ParentId).delete();
            new Document(post.Parent.Parent.ParentId).delete();
           
        }

        #endregion





        #region TestEnsureCorrectDate
        /// <summary>
        /// Tests changing of post date according to parent
        /// </summary>
        [TestMethod]
        public void TestEnsureCorrectDate()
        {
            var post = PostServiceTest.CreatePost();
            // test day folder creation and move
            DateTime minusDay = DateTime.UtcNow.AddDays(-1);
            post.getProperty("uBlogsyPostDate").Value = minusDay;

            DateTime before = DateTime.Parse(post.getProperty("uBlogsyPostDate").Value.ToString());

            // move the node to day-1
            DateFolderService.Instance.EnsureCorrectParentForPost(post);

            // set date back to today
            post.getProperty("uBlogsyPostDate").Value = DateTime.UtcNow;


            // hopefully change date to today-1
            DateFolderService.Instance.EnsureCorrectDate(post);

            // get date after correction
            DateTime after = DateTime.Parse(post.getProperty("uBlogsyPostDate").Value.ToString());


            Assert.AreEqual(before.Year, after.Year);
            Assert.AreEqual(before.Month, after.Month);
            Assert.AreEqual(before.Day, after.Day);

            // cleanup
            //post.delete();
            new Document(post.Parent.Parent.ParentId).delete();
        }


        #endregion




        #region TestGetYearFolder

        /// <summary>
        /// Tests getting of a year folder if it exists.
        /// Tests creating and getting of a year folder.
        /// </summary>
        public void TestGetYearFolder()
        {
            // get landing
            var landing = Document.GetRootDocuments()[0];

            // make a year folder in the future
            DateTime futureDate = DateTime.UtcNow.AddYears(20);
            Inherited.DateFolderService dfs = new Inherited.DateFolderService();

            // get current year - will create a folder if it does not exist!
            Document yearFolder = dfs.TestGetYearFolder(landing, futureDate);

            // assert that year name is correct
            Assert.AreEqual(yearFolder.Text, futureDate.Year.ToString());

            // get landing again, assert that year folder is a child of landing
            landing = new Document(landing.Id);
            Assert.IsTrue(landing.Children.Where(x => x.Id == yearFolder.Id).Count() == 1);

            // assert that year folder has landing as it's parent
            Assert.IsTrue(yearFolder.ParentId == landing.Id);



            // get folder again to show that we can get an existing folder!
            yearFolder = dfs.TestGetYearFolder(landing, futureDate);

            // assert that year name is correct
            Assert.AreEqual(yearFolder.Text, futureDate.Year.ToString());

            // get landing again, assert that year folder is a child of landing
            landing = new Document(landing.Id);
            Assert.IsTrue(landing.Children.Where(x => x.Id == yearFolder.Id).Count() == 1);

            // assert that year folder has landing as it's parent
            Assert.IsTrue(yearFolder.ParentId == landing.Id);

            // clean up
            yearFolder.delete();

            //---

            // make minus year
            DateTime minusYear = futureDate.AddYears(-1);
            yearFolder = dfs.TestGetYearFolder(landing, minusYear);

            // assert that year name is correct
            Assert.AreEqual(yearFolder.Text, minusYear.Year.ToString());

            // get landing again, assert that year folder is a child of landing
            landing = new Document(landing.Id);
            Assert.IsTrue(landing.Children.Where(x => x.Id == yearFolder.Id).Count() == 1);

            // assert that year folder has landing as it's parent
            Assert.IsTrue(yearFolder.ParentId == landing.Id);

            // clean up
            yearFolder.delete();
        }

        #endregion




        #region TestGetMonthFolder

        /// <summary>
        /// Tests getting of a month folder if it exists.
        /// Tests creating and getting of a month folder.
        /// </summary>
        public void TestGetMonthFolder()
        {
            // get landing
            var landing = Document.GetRootDocuments()[0];

            Inherited.DateFolderService dfs = new Inherited.DateFolderService();

            // make a year foler in the future
            DateTime futureDate = DateTime.UtcNow.AddYears(20);
            Document yearFolder = CreateYearFolder(landing, futureDate);


            // create a folder if it does not exist!
            Document monthFolder = dfs.TestGetMonthFolder(yearFolder, futureDate);

            // assert that month name is correct
            Assert.AreEqual(monthFolder.Text, CreateMonthNameWithSelectedFormat(yearFolder, futureDate.Month));

            // get year folder again, assert that month folder is a child of year folder
            yearFolder = new Document(yearFolder.Id);
            Assert.IsTrue(yearFolder.Children.Where(x => x.Id == monthFolder.Id).Count() == 1);

            // assert that month folder has year folder as it's parent
            Assert.IsTrue(monthFolder.ParentId == yearFolder.Id);

            //---

            // get folder again to show that we can get an existing folder!
            monthFolder = dfs.TestGetMonthFolder(yearFolder, futureDate);

            // assert that month name is correct
            Assert.AreEqual(monthFolder.Text, CreateMonthNameWithSelectedFormat(yearFolder, futureDate.Month));


            // get year folder again, assert that month folder is a child of year folder
            yearFolder = new Document(yearFolder.Id);
            Assert.IsTrue(yearFolder.Children.Where(x => x.Id == monthFolder.Id).Count() == 1);

            // assert that month folder has year folder as it's parent
            Assert.IsTrue(monthFolder.ParentId == yearFolder.Id);


            // clean up
            monthFolder.delete();

            //---

            // make minus month
            DateTime minusMonth = futureDate.AddMonths(-1);
            monthFolder = dfs.TestGetMonthFolder(yearFolder, minusMonth);

            // assert that month name is correct
            Assert.AreEqual(monthFolder.Text, CreateMonthNameWithSelectedFormat(yearFolder, minusMonth.Month));

            // get year folder again, assert that month folder is a child of year folder
            yearFolder = new Document(yearFolder.Id);
            Assert.IsTrue(yearFolder.Children.Where(x => x.Id == monthFolder.Id).Count() == 1);

            // assert that month folder has year folder as it's parent
            Assert.IsTrue(monthFolder.ParentId == yearFolder.Id);

            // clean up
            monthFolder.delete();
            yearFolder.delete();
        }


        private string CreateMonthNameWithSelectedFormat(Document yearFolder, int month)
        {
            return (new Inherited.DateFolderService()).TestCreateMonthNameWithSelectedFormat(yearFolder.uBlogsyGetValueFromAncestor("uBlogsyLanding", "uBlogsyGeneralMonthFormat"), month);    
        }


        private Document CreateYearFolder(Document landing, DateTime dateTime)
        {
            return UmbracoAPIHelper.CreateContentNode(dateTime.Year.ToString(), "uBlogsyFolderYear", DocumentService.Instance.GetDefaultDictionary(), landing.Id, true);
        }

        #endregion








        #region TestGetDayFolder

        /// <summary>
        /// Tests getting of a day folder if it exists.
        /// Tests creating and getting of a day folder.
        /// </summary>
        public void TestGetDayFolder()
        {
            // get landing
            var landing = Document.GetRootDocuments()[0];

            Inherited.DateFolderService dfs = new Inherited.DateFolderService();

            // make a year foler in the future
            DateTime futureDate = DateTime.UtcNow.AddYears(20);
            Document yearFolder = CreateYearFolder(landing, futureDate);

            // make a month folder
            Document monthFolder = CreateMonthFolder(yearFolder, futureDate);

            // test creation of day folder
            Document dayFolder = dfs.TestGetDayFolder(monthFolder, futureDate);

            // assert that day name is correct
            Assert.AreEqual(dayFolder.Text, futureDate.Day.ToString());

            // get month folder again, assert that day folder is a child of month folder
            monthFolder = new Document(monthFolder.Id);
            Assert.IsTrue(monthFolder.Children.Where(x => x.Id == dayFolder.Id).Count() == 1);

            // assert that day folder has month folder as it's parent
            Assert.IsTrue(dayFolder.ParentId == monthFolder.Id);

            //---

            // get folder again to show that we can get an existing folder!
            dayFolder = dfs.TestGetDayFolder(monthFolder, futureDate);

            // assert that day name is correct
            Assert.AreEqual(dayFolder.Text, futureDate.Day.ToString());

            // get month folder again, assert that day folder is a child of month folder
            monthFolder = new Document(monthFolder.Id);
            Assert.IsTrue(monthFolder.Children.Where(x => x.Id == dayFolder.Id).Count() == 1);

            // assert that day folder has month folder as it's parent
            Assert.IsTrue(dayFolder.ParentId == monthFolder.Id);


            // clean up
            dayFolder.delete();

            //---

            // make minus day
            DateTime minusDay = futureDate.AddDays(-1);
            dayFolder = dfs.TestGetDayFolder(monthFolder, minusDay);

            // assert that day name is correct
            Assert.AreEqual(dayFolder.Text, minusDay.Day.ToString());

            // get month folder again, assert that day folder is a child of month folder
            monthFolder = new Document(monthFolder.Id);
            Assert.IsTrue(monthFolder.Children.Where(x => x.Id == dayFolder.Id).Count() == 1);

            // assert that day folder has month folder as it's parent
            Assert.IsTrue(dayFolder.ParentId == monthFolder.Id);


            // clean up
            dayFolder.delete();
            monthFolder.delete();
            yearFolder.delete();
        }




        private Document CreateMonthFolder(Document monthFolder, DateTime dateTime)
        {
            return UmbracoAPIHelper.CreateContentNode(dateTime.Month.ToString(), "uBlogsyFolderMonth", DocumentService.Instance.GetDefaultDictionary(), monthFolder.Id, true);
        }


        #endregion





        #region TestCreateMonthNameWithSelectedFormat

        /// <summary>
        /// Tests getting of month name by format
        /// </summary>
        [TestMethod]
        public void TestCreateMonthNameWithSelectedFormat()
        {
            Inherited.DateFolderService dfs = new Inherited.DateFolderService();

            DateTime today = DateTime.UtcNow;

            string monthName = dfs.TestCreateMonthNameWithSelectedFormat("30", today.Month);

            Assert.AreEqual(monthName, today.ToString("MM"));

            monthName = dfs.TestCreateMonthNameWithSelectedFormat("31", today.Month);
            Assert.AreEqual(monthName, today.ToString("M ").Trim());

            monthName = dfs.TestCreateMonthNameWithSelectedFormat("32", today.Month);
            Assert.AreEqual(monthName, today.ToString("MMMM"));

            monthName = dfs.TestCreateMonthNameWithSelectedFormat("33", today.Month);
            Assert.AreEqual(monthName, today.ToString("MMM"));
        }

        
        #endregion





        #region TestCreateDayNameWithSelectedFormat

        /// <summary>
        /// Tests getting of month name by format
        /// </summary>
        [TestMethod]
        public void TestCreateDayNameWithSelectedFormat()
        {
            Inherited.DateFolderService dfs = new Inherited.DateFolderService();

            DateTime today = DateTime.UtcNow;

            string monthName = dfs.TestCreateDayNameWithSelectedFormat("34", today.Day);

            Assert.AreEqual(monthName, today.ToString("dd"));

            monthName = dfs.TestCreateDayNameWithSelectedFormat("35", today.Day);
            Assert.AreEqual(monthName, today.ToString("d").Trim());
        }
        
        #endregion



        //public void TestCreateMember()
        //{
        //    CommentInfo commentInfo = new CommentInfo("Anthony", "anthonydotnet@test.com", "http://mysite.com", "Message", false);

        //    var root = Document.GetRootDocuments()[0];

        //    // create a post
        //    Document post = PostService.Instance.CreatePost(root.Id);
        //    post.getProperty("uBlogsyContentBody").Value = "Content body";

        //    SubscriptionService.Instance.SubscribeToPost(post.Id, commentInfo, true);
        //}

    }
}
