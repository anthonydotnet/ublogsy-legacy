﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using umbraco.cms.businesslogic.web;
using uHelpsy.Core;
using uBlogsy.BusinessLogic;

namespace uBlogsy.Tests
{
    public class TestHelper
    {
        #region public GetRoot

        /// <summary>
        /// Gets the root of the site : nodeTypeAlias == Site
        /// </summary>
        /// <returns></returns>
        public static Document GetRoot()
        {
            var root = Document.GetRootDocuments()[0];
            return root;
        }

        #endregion





        #region public CreateNewsContent

        public static Document CreatePost(DateTime date)
        {
            var root = GetRoot();

            var news = root.Children.Where(x => x.ContentType.Alias == "uBlogsyContainerBlog").Single();

            Document year = null;
            Document month = null;
            Document day = null;

            // get or create year folder
            if (news.Children.Where(x => x.Text == date.Year.ToString()).Count() == 0)
            {
                year = UmbracoAPIHelper.CreateContentNode(date.Year.ToString(), "uBlogsyFolderYear",
                                                          new Dictionary<string, object>() { }, news.Id, true);
            }
            else
            {
                year = news.Children.Where(x => x.Text == date.Year.ToString()).Single();

            }


            // get or create month folder
            if (year.Children.Where(x => x.Text == date.Month.ToString()).Count() == 0)
            {
                month = UmbracoAPIHelper.CreateContentNode(date.ToString("MMM"), "uBlogsyFolderMonth",
                                                           new Dictionary<string, object>() { }, year.Id, true);
            }
            else
            {
                month = year.Children.Where(x => x.Text == date.Month.ToString()).Single();
            }


            // get or create day folder
            if (month.Children.Where(x => x.Text == date.Day.ToString()).Count() == 0)
            {
                day = UmbracoAPIHelper.CreateContentNode(date.Day.ToString(), "uBlogsyFolderDay",
                                                            new Dictionary<string, object>() { }, month.Id, true);
            }
            else
            {
                day = month.Children.Where(x => x.Text == date.Day.ToString()).Single();
            }


            Dictionary<string, object> properties = new Dictionary<string, object>()
                                                        {
                                                            {"uBlogsyContentTitle", "Unit Test Post"},
                                                            {"uBlogsyContentBody", "Unit Test Post Body"},
                                                            {"uBlogsyPostDate", date}
                                                        };

            // create node
            return UmbracoAPIHelper.CreateContentNode("Test Post", "uBlogsyPost", properties, day.Id, true);
        }

        #endregion







    }
}
