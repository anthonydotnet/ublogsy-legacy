﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using umbraco.cms.businesslogic.web;

namespace uBlogsy.Tests.Inherited
{
    public class DateFolderService : uBlogsy.BusinessLogic.DateFolderService
    {
        public Document TestGetCorrectParentForPost(Document doc)
        {
            return GetCorrectParentForPost(doc);
        }


        internal Document TestGetYearFolder(Document root, DateTime date)
        {
            return GetYearFolder(root, date);
        }


        internal Document TestGetMonthFolder(Document yearFolder, DateTime date)
        {
            return GetMonthFolder(yearFolder, date);
        }

        internal Document TestGetDayFolder(Document monthFolder, DateTime date)
        {
            return GetDayFolder(monthFolder, date);
        }


  
        
            
            
            
            
            ///// <summary>
        ///// Used to create or get a year folder so we can test month folder.
        ///// </summary>
        ///// <param name="root"></param>
        ///// <param name="date"></param>
        ///// <returns></returns>
        //public Document GetYearFolder(Document root, DateTime date)
        //{
        //    return GetYearFolder(root, date);
        //}


        ///// <summary>
        ///// Used to create or get a month folder so we can test day folder.
        ///// </summary>
        ///// <param name="root"></param>
        ///// <param name="date"></param>
        ///// <returns></returns>
        //public Document GetMonthFolder(Document yearFolder, DateTime date)
        //{
        //    return GetMonthFolder(yearFolder, date);
        //}

    }
}
