﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Examine.LuceneEngine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using uBlogsy.BusinessLogic;
using uBlogsy.BusinessLogic.Models;
using uBlogsy.Common.Helpers;
using uHelpsy.Core;
using umbraco.cms.businesslogic.web;
using uBlogsy.BusinessLogic.Extensions;

namespace uBlogsy.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class DateFolderServiceTest
    {
        public DateFolderServiceTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        protected TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion






        #region TestUseAutoSorting

        /// <summary>
        /// Tests getting of "use auto sorting"
        /// </summary>
        [TestMethod]
        public void UseAutoDateFolders()
        {
            bool result = ConfigReader.Instance.UseAutoDateFolders();
            Assert.IsFalse(result);
        }

        #endregion









        #region TestEnsureCorrectParentForPost
        /// <summary>
        /// Tests moving of node to correct parent according to date.
        /// Differs to TestGetCorrectParentForPost() because we are getting the parent AFTER the post is moved.
        /// </summary>
        [TestMethod]
        public void TestEnsureCorrectParentForPost()
        {
            DateTime date = new DateTime(2000, 1, 1);
            Document post = TestHelper.CreatePost(date);
            Document oldParent = null;
            try
            {
                // test post has correct parent
                post = DateFolderService.Instance.EnsureCorrectParentForPost(post);

                Document parent = new Document(post.ParentId);

                // assert that parent is a day folder
                Assert.AreEqual(parent.ContentType.Alias, "uBlogsyFolderDay");

                // test day folder creation and move
                DateTime minusDay = date.AddDays(-1);
                post.getProperty("uBlogsyPostDate").Value = minusDay;

                oldParent = new Document(post.ParentId);
                DateFolderService.Instance.EnsureCorrectParentForPost(post);

                // re-get post
                post = new Document(post.Id);
                parent = new Document(post.ParentId);
                Assert.AreEqual(parent.Text, minusDay.Day.ToString());
                Assert.AreNotEqual(oldParent.Id, parent.Id);
                //------

                // test day folder move back
                DateTime thisDay = date;
                post.getProperty("uBlogsyPostDate").Value = thisDay;
                oldParent = new Document(post.ParentId);
                DateFolderService.Instance.EnsureCorrectParentForPost(post);

                // re-get post
                post = new Document(post.Id);
                parent = new Document(post.ParentId);
                Assert.AreEqual(parent.Text, thisDay.Day.ToString());
                Assert.AreNotEqual(oldParent.Id, parent.Id);
                //------

                // test month folder creation and move
                DateTime minusMonth = date.AddMonths(-1);
                post.getProperty("uBlogsyPostDate").Value = minusMonth;
                oldParent = new Document(post.ParentId);
                DateFolderService.Instance.EnsureCorrectParentForPost(post);

                // re-get post
                post = new Document(post.Id);
                parent = new Document(post.ParentId);
                Assert.AreEqual(parent.Text, minusMonth.Day.ToString());
                Assert.AreNotEqual(oldParent.Id, parent.Id);
                //------

                // test month folder move back
                DateTime thisMonth = date;
                post.getProperty("uBlogsyPostDate").Value = thisMonth;
                oldParent = new Document(post.ParentId);
                DateFolderService.Instance.EnsureCorrectParentForPost(post);

                // re-get post
                post = new Document(post.Id);
                parent = new Document(post.ParentId);
                Assert.AreEqual(parent.Text, thisMonth.Day.ToString());
                Assert.AreNotEqual(oldParent.Id, parent.Id);
                //------

                // test year folder creation and move
                DateTime minusYear = date.AddYears(-1);
                post.getProperty("uBlogsyPostDate").Value = minusYear;
                oldParent = new Document(post.ParentId);
                DateFolderService.Instance.EnsureCorrectParentForPost(post);

                // re-get post
                post = new Document(post.Id);
                parent = new Document(post.ParentId);
                Assert.AreEqual(parent.Text, minusYear.Day.ToString());
                Assert.AreNotEqual(oldParent.Id, parent.Id);
                //------

                // now move node back to existing
                // test year folder move back
                DateTime thisYear = date;
                post.getProperty("uBlogsyPostDate").Value = thisYear;
                oldParent = new Document(post.ParentId);
                DateFolderService.Instance.EnsureCorrectParentForPost(post);

                // re-get post
                post = new Document(post.Id);
                parent = new Document(post.ParentId);
                Assert.AreEqual(parent.Text, thisYear.Day.ToString());
                Assert.AreNotEqual(oldParent.Id, parent.Id);
                //------
            }
            catch { throw; }
            finally
            {
                // cleanup
                // clean up
                new Document(oldParent.Parent.ParentId).delete();
                new Document(post.Parent.Parent.ParentId).delete();
            }
        }


        #endregion





        #region TestGetCorrectParentForPost

        /// <summary>
        /// Tests moving of node to correct parent according to date.
        /// Differs to TestEnsureCorrectParentForPost() because we are testing a "get" of the new parent BEFORE the post is moved.
        /// 
        /// This is the worst test method ever! What were we thinking?
        /// </summary>
        [TestMethod]
        public void TestGetCorrectParentForPost()
        {
            // ensures that date folder is not enabled during create of test node
            DisableDateFolders();

            
            // create a post
            DateTime date = new DateTime(2000, 1, 1);
            Document post = TestHelper.CreatePost(date);


            Inherited.DateFolderService dfs = new Inherited.DateFolderService();
            Document oldParent = null;
            Document newParent = null;
            try
            {
                // move the post to correct parent - involes creation of parents
                newParent = dfs.TestGetCorrectParentForPost(post);
                post = new Document(post.Id);

                // change date to yesterday
                DateTime minusDay = date.AddDays(-1);
                post.getProperty("uBlogsyPostDate").Value = minusDay;

                oldParent = new Document(post.ParentId);

                // move post to yesterday
                newParent = dfs.TestGetCorrectParentForPost(post);

                // test new parent name
                Assert.AreEqual(newParent.Text, minusDay.ToString(ConfigReader.Instance.GetDayFormat()));

                // assert that new parent is not same as old parent
                Assert.AreNotEqual(newParent.Id, oldParent.Id);


                //------

                // test day folder move back
                DateTime thisDay = date;
                post.getProperty("uBlogsyPostDate").Value = thisDay;

                oldParent = newParent;

                // move post back to today
                newParent = dfs.TestGetCorrectParentForPost(post);

                // test new parent name
                Assert.AreEqual(newParent.Text, thisDay.ToString(ConfigReader.Instance.GetDayFormat()));

                // assert that new parent is not same as old parent
                Assert.AreNotEqual(newParent.Id, oldParent.Id);

                //------

                // test month folder creation and move
                DateTime minusMonth = date.AddMonths(-1);
                post.getProperty("uBlogsyPostDate").Value = minusMonth;

                oldParent = newParent;

                // move post to last month
                newParent = dfs.TestGetCorrectParentForPost(post);


                // test new parent name
                Assert.AreEqual(newParent.Text, minusMonth.ToString(ConfigReader.Instance.GetDayFormat()));

                // assert that new parent is not same as old parent
                Assert.AreNotEqual(newParent.Id, oldParent.Id);
                //------


                // test month folder move back
                DateTime thisMonth = date;
                post.getProperty("uBlogsyPostDate").Value = thisMonth;

                oldParent = newParent;

                // move post back to current month
                newParent = dfs.TestGetCorrectParentForPost(post);

                // test new parent name
                Assert.AreEqual(newParent.Text, thisMonth.ToString(ConfigReader.Instance.GetDayFormat()));

                // assert that new parent is not same as old parent
                Assert.AreNotEqual(newParent.Id, oldParent.Id);

                //------

                // test year folder creation and move
                DateTime minusYear = date.AddYears(-1);
                post.getProperty("uBlogsyPostDate").Value = minusYear;

                oldParent = newParent;

                // move post to last year
                newParent = dfs.TestGetCorrectParentForPost(post);

                // test new parent name
                Assert.AreEqual(newParent.Text, minusYear.ToString(ConfigReader.Instance.GetDayFormat()));

                // assert that new parent is not same as old parent
                Assert.AreNotEqual(newParent.Id, oldParent.Id);
                //------

                // now move node back to existing year
                // test year folder move back
                DateTime thisYear = date;
                post.getProperty("uBlogsyPostDate").Value = thisYear;

                oldParent = newParent;

                // move post back to this year
                newParent = dfs.TestGetCorrectParentForPost(post);

                // test new parent name
                Assert.AreEqual(newParent.Text, thisYear.ToString(ConfigReader.Instance.GetDayFormat()));

                // assert that new parent is not same as old parent
                Assert.AreNotEqual(newParent.Id, oldParent.Id);
            }
            catch { throw; }
            finally
            {
                // clean up
                new Document(oldParent.Parent.ParentId).delete();
                new Document(post.Parent.Parent.ParentId).delete();
            }
        }

        #endregion






        #region TestEnsureCorrectDate
        /// <summary>
        /// Tests changing of post date according to parent
        /// </summary>
        [TestMethod]
        public void TestEnsureCorrectDate()
        {
            // ensures that date folder is not enabled during create of test node
            DisableDateFolders();


            DateTime date = new DateTime(2000, 1, 1);
            Document post = TestHelper.CreatePost(date);

          

            try
            {
                // test day change 
                DateTime newDate = date.AddDays(1);
                post.getProperty("uBlogsyPostDate").Value = newDate;
                post = DateFolderService.Instance.EnsureCorrectDate(post);

                // get date after correction
                DateTime actual = DateTime.Parse(post.getProperty("uBlogsyPostDate").Value.ToString());

                Assert.AreEqual(date.Year, actual.Year);
                Assert.AreEqual(date.Month, actual.Month);
                Assert.AreEqual(date.Day, actual.Day);

                //--

                // test day and month change
                newDate = new DateTime(date.Year, date.Month + 1, date.Day + 1);
                post.getProperty("uBlogsyPostDate").Value = newDate;

                post = DateFolderService.Instance.EnsureCorrectDate(post);

                actual = DateTime.Parse(post.getProperty("uBlogsyPostDate").Value.ToString());

                Assert.AreEqual(date.Year, actual.Year);
                Assert.AreEqual(date.Month, actual.Month);
                Assert.AreEqual(date.Day, actual.Day);


                //--

                // test day, month, year change
                newDate = new DateTime(date.Year + 1, date.Month + 1, date.Day + 1);
                post.getProperty("uBlogsyPostDate").Value = newDate;

                post = DateFolderService.Instance.EnsureCorrectDate(post);
                actual = DateTime.Parse(post.getProperty("uBlogsyPostDate").Value.ToString());

                Assert.AreEqual(date.Year, actual.Year);
                Assert.AreEqual(date.Month, actual.Month);
                Assert.AreEqual(date.Day, actual.Day);
            }
            catch
            {
                throw;
            }
            finally
            {
                // cleanup
                //post.delete();
                new Document(post.Parent.Parent.ParentId).delete();
            }
        }


        private void DisableDateFolders()
        {
            var configString = "<uBlogsySettings>\n"
                               + "  <dateFolders enabled=\"false\">\n"
                               + "      <!-- Valid values: M MM MMM MMMM == 3 03 Mar March -->\n"
                               + "      <monthFormat>MMMM</monthFormat>\n"
                               + "      <!-- Valid formats: d, dd == 9, 09 respectively -->\n"
                               + "      <dayFormat>dd</dayFormat>\n"
                               + "  </dateFolders>\n"
                               + "  <comments>\n"
                               + "      <!-- send notification email to subscribers when you re-publish a comment from the back office - set to true or false -->\n"
                               + "      <sendNotificationOnRepublish>true</sendNotificationOnRepublish>\n"
                               + "  </comments>\n"
                               + "</uBlogsySettings>\n";

            File.WriteAllText(HttpContext.Current.Server.MapPath("~/config/uBlogsy.config"), configString);
        }

        #endregion





        #region TestGetYearFolder

        /// <summary>
        /// Tests getting of a year folder if it exists.
        /// Tests creating and getting of a year folder.
        /// </summary>
        public void TestGetYearFolder()
        {
            // get landing
            var landing = Document.GetRootDocuments()[0];
            DateTime date = new DateTime(2000, 1, 1);
            Inherited.DateFolderService dfs = new Inherited.DateFolderService();
            Document yearFolder = null;

            try
            {
                // get current year - will create a folder if it does not exist!
                yearFolder = dfs.TestGetYearFolder(landing, date);

                // assert that year name is correct
                Assert.AreEqual(yearFolder.Text, date.Year.ToString());

                // get landing again, assert that year folder is a child of landing
                landing = new Document(landing.Id);
                Assert.IsTrue(landing.Children.Where(x => x.Id == yearFolder.Id).Count() == 1);

                // assert that year folder has landing as it's parent
                Assert.IsTrue(yearFolder.ParentId == landing.Id);

                // get folder again to show that we can get an existing folder!
                yearFolder = dfs.TestGetYearFolder(landing, date);

                // assert that year name is correct
                Assert.AreEqual(yearFolder.Text, date.Year.ToString());

                // get landing again, assert that year folder is a child of landing
                landing = new Document(landing.Id);
                Assert.IsTrue(landing.Children.Where(x => x.Id == yearFolder.Id).Count() == 1);

                // assert that year folder has landing as it's parent
                Assert.IsTrue(yearFolder.ParentId == landing.Id);
            }
            catch
            {
                throw;
            }
            finally
            {
                // clean up
                yearFolder.delete();
            }
        }

        #endregion






        #region TestGetMonthFolder

        /// <summary>
        /// Tests getting of a month folder if it exists.
        /// Tests creating and getting of a month folder.
        /// </summary>
        public void TestGetMonthFolder()
        {
            // get landing
            var landing = Document.GetRootDocuments()[0];
            Inherited.DateFolderService dfs = new Inherited.DateFolderService();

            DateTime date = new DateTime(2000, 1, 1);

            // make a year foler in the future
            Document yearFolder = CreateYearFolder(landing, date);
            Document monthFolder = null;

            try
            {
                monthFolder = dfs.TestGetMonthFolder(yearFolder, date);

                // assert that month name is correct
                Assert.AreEqual(monthFolder.Text, DateHelper.GetMonthNameWithFormat(date.Month, ConfigReader.Instance.GetMonthFormat()));

                // get year folder again, assert that month folder is a child of year folder
                yearFolder = new Document(yearFolder.Id);
                Assert.IsTrue(yearFolder.Children.Where(x => x.Id == monthFolder.Id).Count() == 1);

                // assert that month folder has year folder as it's parent
                Assert.IsTrue(monthFolder.ParentId == yearFolder.Id);

                //---

                // get folder again to show that we can get an existing folder!
                monthFolder = dfs.TestGetMonthFolder(yearFolder, date);

                // assert that month name is correct
                Assert.AreEqual(monthFolder.Text, DateHelper.GetMonthNameWithFormat(date.Month, ConfigReader.Instance.GetMonthFormat()));

                // get year folder again, assert that month folder is a child of year folder
                yearFolder = new Document(yearFolder.Id);
                Assert.IsTrue(yearFolder.Children.Where(x => x.Id == monthFolder.Id).Count() == 1);

                // assert that month folder has year folder as it's parent
                Assert.IsTrue(monthFolder.ParentId == yearFolder.Id);

            }
            catch { throw; }
            finally
            {
                // clean up
                yearFolder.delete();
            }
        }




        private Document CreateYearFolder(Document landing, DateTime dateTime)
        {
            return UmbracoAPIHelper.CreateContentNode(dateTime.Year.ToString(), "uBlogsyFolderYear", new Dictionary<string, object>(), landing.Id, true);
        }

        #endregion







        #region TestGetDayFolder

        /// <summary>
        /// Tests getting of a day folder if it exists.
        /// Tests creating and getting of a day folder.
        /// </summary>
        public void TestGetDayFolder()
        {
            // get landing
            var landing = Document.GetRootDocuments()[0];

            Inherited.DateFolderService dfs = new Inherited.DateFolderService();
            DateTime date = new DateTime(2000, 1, 1);
            Document yearFolder = CreateYearFolder(landing, date);
            Document monthFolder = CreateMonthFolder(yearFolder, date);
            Document dayFolder;

            try
            {
                dayFolder = dfs.TestGetDayFolder(monthFolder, date);

                // assert that day name is correct
                Assert.AreEqual(dayFolder.Text, date.ToString(ConfigReader.Instance.GetDayFormat()));

                // get month folder again, assert that day folder is a child of month folder
                monthFolder = new Document(monthFolder.Id);
                Assert.IsTrue(monthFolder.Children.Where(x => x.Id == dayFolder.Id).Count() > 0);

                // assert that day folder has month folder as it's parent
                Assert.IsTrue(dayFolder.ParentId == monthFolder.Id);

                //---

                // get folder again to show that we can get an existing folder!
                dayFolder = dfs.TestGetDayFolder(monthFolder, date);

                // assert that day name is correct
                Assert.AreEqual(dayFolder.Text, date.ToString(ConfigReader.Instance.GetDayFormat()));

                // get month folder again, assert that day folder is a child of month folder
                monthFolder = new Document(monthFolder.Id);
                Assert.IsTrue(monthFolder.Children.Where(x => x.Id == dayFolder.Id).Count() > 0);

                // assert that day folder has month folder as it's parent
                Assert.IsTrue(dayFolder.ParentId == monthFolder.Id);
            }
            catch { throw; }
            finally
            {
                // clean up
                yearFolder.delete();
            }
        }




        private Document CreateMonthFolder(Document monthFolder, DateTime dateTime)
        {
            return UmbracoAPIHelper.CreateContentNode(dateTime.Month.ToString(), "uBlogsyFolderMonth", GetDefaultDictionary(), monthFolder.Id, true);
        }


        #endregion



        #region GetDefaultDictionaryB

        /// <summary>
        /// Returns dictionary with default values
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, object> GetDefaultDictionary()
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>() 
                    { 
                        { "uBlogsyContentTitle", string.Empty }, 
                        { "uBlogsyContentBody", string.Empty },
                        { "uBlogsyContentSummary", string.Empty },
                        { "uBlogsySeoKeywords", string.Empty},
                        { "uBlogsySeoDescription", string.Empty},
                        { "umbracoNaviHide", 0},
                        { "umbracoRedirect", string.Empty},
                        { "umbracoUrlName", string.Empty},
                        { "umbracoInternalRedirectId", string.Empty},
                        { "umbracoUrlAlias", string.Empty}
                    };
            return dictionary;
        }

        #endregion


    }
}
