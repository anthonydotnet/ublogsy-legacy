﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using umbraco.cms.businesslogic.web;
using uBlogsy.BusinessLogic;
using uHelpsy.Core;
using umbraco.MacroEngines;
using umbraco.BusinessLogic;

namespace uBlogsy.Tests
{
    [TestClass]
    public class PostServiceTest
    {

        protected const string CONTENT_TITLE = "Content Title";
        protected const string CONTENT_BODY = "Content Body";
        protected static DateTime POST_DATE = new DateTime(2011, 1, 1);


      




        #region CreatePostTest

     



        protected List<Document> SetupForSearch()
        {
            DateTime date1 = new DateTime(2011, 1, 1);
            DateTime date2 = new DateTime(2011, 2, 1);
            DateTime date3 = new DateTime(2011, 3, 1);
            DateTime date4 = new DateTime(2011, 4, 1);

            DateTime date1_2 = new DateTime(2011, 1, 2);
            DateTime date2_2 = new DateTime(2011, 2, 3);
            DateTime date3_2 = new DateTime(2011, 3, 4);
            DateTime date4_2 = new DateTime(2011, 4, 5);


            DateTime date5 = new DateTime(2010, 1, 1);
            DateTime date6 = new DateTime(2010, 2, 1);
            DateTime date7 = new DateTime(2010, 3, 1);
            DateTime date8 = new DateTime(2010, 4, 1);

            DateTime date5_2 = new DateTime(2010, 1, 2);
            DateTime date6_2 = new DateTime(2010, 2, 3);
            DateTime date7_2 = new DateTime(2010, 3, 4);
            DateTime date8_2 = new DateTime(2010, 4, 5);


            List<Document> docs = new List<Document>();
            int postsPerDate = 1;

            docs.AddRange(CreatePosts(date1, postsPerDate));
            docs.AddRange(CreatePosts(date2, postsPerDate));
            docs.AddRange(CreatePosts(date3, postsPerDate));
            docs.AddRange(CreatePosts(date4, postsPerDate));
            //docs.AddRange(CreatePosts(date5, postsPerDate));
            //docs.AddRange(CreatePosts(date6, postsPerDate));
            //docs.AddRange(CreatePosts(date7, postsPerDate));
            //docs.AddRange(CreatePosts(date8, postsPerDate));

            //docs.AddRange(CreatePosts(date1_2, postsPerDate));
            //docs.AddRange(CreatePosts(date2_2, postsPerDate));
            //docs.AddRange(CreatePosts(date3_2, postsPerDate));
            //docs.AddRange(CreatePosts(date4_2, postsPerDate));
            //docs.AddRange(CreatePosts(date5_2, postsPerDate));
            //docs.AddRange(CreatePosts(date6_2, postsPerDate));
            //docs.AddRange(CreatePosts(date7_2, postsPerDate));
            //docs.AddRange(CreatePosts(date8_2, postsPerDate));


            return docs;
        }


        protected void CleanUp(List<Document> docs )
        {
            foreach (var d in docs)
            {
                new Document(d.Parent.Parent.ParentId).delete();
            }
        }


        protected List<Document> CreatePosts(DateTime date, int noOfPost)
        {
            List<Document> docs = new List<Document>();

            string title = "Test Post";
            string body = "Test Post Body";

            for (int i = 0; i < noOfPost; i++)
            {
                var doc = TestHelper.CreatePost(date);
                doc.getProperty("uBlogsyContentTitle").Value = title + " " + i;
                doc.getProperty("uBlogsyContentBody").Value = body + " " + i;

                doc.getProperty("uBlogsyPostDate").Value = date;
                

                doc.Publish(new User(0));
                docs.Add(doc);
            }
            return docs;
        }





        /// <summary>
        ///A test for CreatePost
        ///</summary>
        [TestMethod()]
        public void CreatePostTest()
        {
            PostService_Accessor target = new PostService_Accessor(); 

            var root = Document.GetRootDocuments()[0];

            var post = target.CreatePost(root.Id);

            post.getProperty("uBlogsyContentTitle").Value = CONTENT_TITLE;
            post.getProperty("uBlogsyContentBody").Value = CONTENT_BODY;

            // get posted
            Document posted = new Document(post.Id);

            Assert.AreEqual(posted.getProperty("uBlogsyContentTitle").Value, CONTENT_TITLE);
            Assert.AreEqual(posted.getProperty("uBlogsyContentBody").Value, CONTENT_BODY);
            Assert.AreEqual(posted.Id, post.Id);
        }


        #endregion

        /// <summary>
        ///A test for DoSearch
        ///</summary>
        [TestMethod()]
        [DeploymentItem("uBlogsy.BusinessLogic.dll")]
        public void DoSearchTest()
        {
            PostService_Accessor target = new PostService_Accessor(); 
            string searchTerm = "Test";

            List<Document> docs  = SetupForSearch();

            IEnumerable<DynamicNode> sorted = docs.Select(x => new DynamicNode(x.Id)); 
            IEnumerable<DynamicNode> actual;
            
            actual = target.DoSearch(TestHelper.GetRoot().Id, searchTerm, sorted);

            Assert.AreEqual(actual.Count(), 4);

            CleanUp(docs);

        }





        /// <summary>
        ///A test for GetAuthors
        ///</summary>
        [TestMethod()]
        public void GetAuthorsTest()
        {
            PostService_Accessor target = new PostService_Accessor(); // TODO: Initialize to an appropriate value
            int postId = 0; // TODO: Initialize to an appropriate value
            IEnumerable<string> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<string> actual;
            actual = target.GetAuthors(postId);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetIndexOf
        ///</summary>
        [TestMethod()]
        [DeploymentItem("uBlogsy.BusinessLogic.dll")]
        public void GetIndexOfTest()
        {
            PostService_Accessor target = new PostService_Accessor(); // TODO: Initialize to an appropriate value
            List<DynamicNode> siblings = null; // TODO: Initialize to an appropriate value
            DynamicNode current = null; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.GetIndexOf(siblings, current);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

       
        /// <summary>
        ///A test for GetNextPost
        ///</summary>
        [TestMethod()]
        public void GetNextPostTest()
        {
            PostService_Accessor target = new PostService_Accessor(); // TODO: Initialize to an appropriate value
            DynamicNode current = null; // TODO: Initialize to an appropriate value
            DynamicNode expected = null; // TODO: Initialize to an appropriate value
            DynamicNode actual;
            actual = target.GetNextPost(current);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetPostSiblings
        ///</summary>
        [TestMethod()]
        [DeploymentItem("uBlogsy.BusinessLogic.dll")]
        public void GetPostSiblingsTest()
        {
            //PostService_Accessor target = new PostService_Accessor(); // TODO: Initialize to an appropriate value
            //DynamicNode current = null; // TODO: Initialize to an appropriate value
            //List<DynamicNode> expected = null; // TODO: Initialize to an appropriate value
            //List<DynamicNode> actual;
            //actual = target.GetPostSiblings(current);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetPosts
        ///</summary>
        [TestMethod()]
        [DeploymentItem("uBlogsy.BusinessLogic.dll")]
        public void GetPostsTest()
        {
            //string item = string.Empty; // TODO: Initialize to an appropriate value
            //string alias = string.Empty; // TODO: Initialize to an appropriate value
            //IEnumerable<DynamicNode> sorted = null; // TODO: Initialize to an appropriate value
            //List<DynamicNode> expected = null; // TODO: Initialize to an appropriate value
            //List<DynamicNode> actual;
            //actual = PostService_Accessor.GetPosts(item, alias, sorted);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }





        /// <summary>
        ///A test for GetPosts
        ///</summary>
        [TestMethod()]
        public void GetPostsTest1()
        {
            PostService_Accessor target = new PostService_Accessor(); // TODO: Initialize to an appropriate value
            int postId = 0; // TODO: Initialize to an appropriate value
            string tag = string.Empty; // TODO: Initialize to an appropriate value
            string category = string.Empty; // TODO: Initialize to an appropriate value
            string author = string.Empty; // TODO: Initialize to an appropriate value
            string searchTerm = string.Empty; // TODO: Initialize to an appropriate value
            string commenter = string.Empty; // TODO: Initialize to an appropriate value
            IEnumerable<DynamicNode> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<DynamicNode> actual;
            actual = target.GetPosts(postId, tag, category, author, searchTerm, commenter, 0, int.MaxValue);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetPostsByCommenter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("uBlogsy.BusinessLogic.dll")]
        public void GetPostsByCommenterTest()
        {
            PostService_Accessor target = new PostService_Accessor(); // TODO: Initialize to an appropriate value
            string commenter = string.Empty; // TODO: Initialize to an appropriate value
            IEnumerable<DynamicNode> postList = null; // TODO: Initialize to an appropriate value
            IEnumerable<DynamicNode> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<DynamicNode> actual;
            actual = target.GetPostsByCommenter(commenter, postList);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetPreviousPost
        ///</summary>
        [TestMethod()]
        public void GetPreviousPostTest()
        {
            PostService_Accessor target = new PostService_Accessor(); // TODO: Initialize to an appropriate value
            DynamicNode current = null; // TODO: Initialize to an appropriate value
            DynamicNode expected = null; // TODO: Initialize to an appropriate value
            DynamicNode actual;
            actual = target.GetPreviousPost(current);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetRelatedPosts
        ///</summary>
        [TestMethod()]
        [DeploymentItem("uBlogsy.BusinessLogic.dll")]
        public void GetRelatedPostsTest()
        {
            PostService_Accessor target = new PostService_Accessor(); // TODO: Initialize to an appropriate value
            int postId = 0; // TODO: Initialize to an appropriate value
            string itemAlias = string.Empty; // TODO: Initialize to an appropriate value
            IEnumerable<DynamicNode> posts = null; // TODO: Initialize to an appropriate value
            int matchCount = 0; // TODO: Initialize to an appropriate value
            IEnumerable<DynamicNode> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<DynamicNode> actual;
            actual = target.GetRelatedPosts(postId, itemAlias, posts, matchCount);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetRelatedPosts
        ///</summary>
        [TestMethod()]
        public void GetRelatedPostsTest1()
        {
            PostService_Accessor target = new PostService_Accessor(); // TODO: Initialize to an appropriate value
            int postId = 0; // TODO: Initialize to an appropriate value
            string itemAlias = string.Empty; // TODO: Initialize to an appropriate value
            int matchCount = 0; // TODO: Initialize to an appropriate value
            IEnumerable<DynamicNode> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<DynamicNode> actual;
            actual = target.GetRelatedPosts(postId, itemAlias, matchCount);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Instance
        ///</summary>
        [TestMethod()]
        public void InstanceTest()
        {
            PostService actual;
            actual = PostService.Instance;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
