﻿using uBlogsy.BusinessLogic.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using umbraco.cms.businesslogic.web;

namespace uBlogsy.Tests
{
    
    
    /// <summary>
    ///This is a test class for DocumentExtensionsTest and is intended
    ///to contain all DocumentExtensionsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DocumentExtensionsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for uBlogsyGetValueFromAncestor
        ///</summary>
        [TestMethod()]
        public void uBlogsyGetValueFromAncestorTest()
        {
            string title = "My Blog Title";

            var landing = Document.GetRootDocuments()[0];
            landing.getProperty("uBlogsyContentTitle").Value = title;
            DateTime date = new DateTime(2000, 1, 1);
            Document doc = TestHelper.CreatePost(date);


            string ancestorAlias = "uBlogsyLanding";
            string propertyAlias = "uBlogsyContentTitle";
            string expected = title;

            string actual = DocumentExtensions.uBlogsyGetValueFromAncestor(doc, ancestorAlias, propertyAlias);
            Assert.AreEqual(expected, actual);
        }
    }
}
