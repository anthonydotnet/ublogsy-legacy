﻿using uBlogsy.BusinessLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace uBlogsy.Tests
{
    
    
    /// <summary>
    ///This is a test class for ConfigReaderTest and is intended
    ///to contain all ConfigReaderTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ConfigReaderTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for UseAutoDateFolders
        ///</summary>
        [TestMethod()]
        public void UseAutoDateFoldersTest()
        {
            ConfigReader_Accessor target = new ConfigReader_Accessor(); 
            bool expected = false; 
            bool actual;
            actual = target.UseAutoDateFolders();
            Assert.AreEqual(expected, actual);
        }
    }
}
