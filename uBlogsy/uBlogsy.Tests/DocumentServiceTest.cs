﻿using uBlogsy.BusinessLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using umbraco.cms.businesslogic.web;
using System.Collections.Generic;
using System.Linq;
namespace uBlogsy.Tests
{


    /// <summary>
    ///This is a test class for DocumentServiceTest and is intended
    ///to contain all DocumentServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DocumentServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        ///// <summary>
        /////A test for EnsureCommentsFolder
        /////</summary>
        //[TestMethod()]
        //public void EnsureCommentsFolderTest()
        //{
        //    DateTime date = new DateTime(2000, 1, 1);
        //    CommentService_Accessor target = new CommentService_Accessor();
        //    Document doc = TestHelper.CreatePost(date);
        //    doc = target.EnsureCommentsFolder(doc);
        //    Assert.IsTrue(doc.Children.Where(x => x.ContentType.Alias == "uBlogsyContainerComment").Count() == 1);

        //    doc.delete();
        //}


        /// <summary>
        ///A test for EnsureCorrectPostNodeName
        ///</summary>
        [TestMethod()]
        public void EnsureCorrectPostNodeNameTest()
        {
            DateTime date = new DateTime(2000, 1, 1);
            DocumentService_Accessor target = new DocumentService_Accessor();
            Document post = TestHelper.CreatePost(date);

            // get root
            var landing = Document.GetRootDocuments()[0];

            // set landing property to make node name same as title 
            landing.getProperty("uBlogsyGeneralUseTitleAsNodeName").Value = "1";

            // set node name
            post = target.EnsureCorrectPostNodeName(post);

            // title is empty on create!
            string title = post.getProperty("uBlogsyContentTitle").Value.ToString();
            string nodeName = post.Text;

            Assert.AreEqual(title, nodeName);
            //Assert.AreEqual(title, string.Empty);

            // set title
            landing.getProperty("uBlogsyGeneralUseTitleAsNodeName").Value = "0";
            post.getProperty("uBlogsyContentTitle").Value = "TEST";
            post = target.EnsureCorrectPostNodeName(post);

            title = post.getProperty("uBlogsyContentTitle").Value.ToString();
            nodeName = post.Text;

            Assert.AreNotEqual(title, nodeName);



            // clean up
            post.delete();

            // create new post
            post = TestHelper.CreatePost(date);

            // set landing property to make node name same as title 
            landing.getProperty("uBlogsyGeneralUseTitleAsNodeName").Value = "0";

            title = post.getProperty("uBlogsyContentTitle").Value.ToString();
            nodeName = post.Text;

            Assert.AreNotEqual(title, nodeName);


            // clean up
            landing.getProperty("uBlogsyGeneralUseTitleAsNodeName").Value = "1";
            post.delete();


            //// we are not taking care of moving nodes with same name
            //// create 2 nodes
            //post = TestHelper.CreatePost(date);
            //var post2 = TestHelper.CreatePost(date);
            //Assert.AreNotEqual(post.Text, post2.Text);
        }



        /// <summary>
        ///A test for EnsureNodeExists
        ///</summary>
        [TestMethod()]
        public void EnsureNodeExistsTest()
        {
            DocumentService_Accessor target = new DocumentService_Accessor();

            var landing = Document.GetRootDocuments()[0];

            Document doc = null;
            string alias = "uBlogsyPost";
            string name = "Test";
            bool publish = false;
            doc = target.EnsureNodeExists(landing.Id, doc, alias, name, publish);

            Assert.IsNotNull(doc);

            // cleanup
            doc.delete();
        }





        /// <summary>
        ///A test for GetDefaultDictionary
        ///</summary>
        [TestMethod()]
        public void GetDefaultDictionaryTest()
        {
            DocumentService_Accessor target = new DocumentService_Accessor();
            Dictionary<string, object> expected = new Dictionary<string, object>() 
                    { 
                        { "uBlogsyContentTitle", string.Empty }, 
                        { "uBlogsyContentBody", string.Empty },
                        { "uBlogsyContentSummary", string.Empty },
                        { "uBlogsySeoKeywords", string.Empty},
                        { "uBlogsySeoDescription", string.Empty},
                        { "umbracoNaviHide", 0},
                        { "umbracoRedirect", string.Empty},
                        { "umbracoUrlName", string.Empty},
                        { "umbracoInternalRedirectId", string.Empty},
                        { "umbracoUrlAlias", string.Empty}
                    };

            Dictionary<string, object> actual;
            actual = DateFolderServiceTest.GetDefaultDictionary();

            foreach (var key in expected.Keys)
            {
                Assert.AreEqual(expected[key], actual[key]);
            }
        }



        /// <summary>
        ///A test for GetDocumentByAlias
        ///</summary>
        [TestMethod()]
        public void GetDocumentByAliasTest()
        {
            DocumentService_Accessor target = new DocumentService_Accessor();


            string rootAlias = "uBlogsyLanding";
            string alias = "uBlogsyLanding";

            // get root
            var landing = Document.GetRootDocuments()[0];

            var current = landing;

            // test when current is landing
            Document actual = target.GetDocumentByAlias(current, rootAlias, alias);

            // assert that we returned landing
            Assert.AreEqual(landing.Id, actual.Id);

            // go deeper into tree
            var list = landing.GetDescendants().Cast<Document>().First();

            // test when current is landing
            actual = target.GetDocumentByAlias(current, rootAlias, alias);

            // assert that we returned landing
            Assert.AreEqual(landing.Id, actual.Id);
        }







        public void DeleteAllPosts() 
        {

            var roots = Document.GetRootDocuments();
            
            var nodes = TestHelper.GetRoot().GetDescendants().Cast<Document>().Where(x => x.ContentType.Alias == "uBlogsyPost");

            foreach (var doc in nodes)
            {
                doc.delete();
            }

            nodes = TestHelper.GetRoot().GetDescendants().Cast<Document>().Where(x => x.ContentType.Alias == "uBlogsyFolderDay");
            foreach (var doc in nodes)
            {
                doc.delete();
            }

            nodes = TestHelper.GetRoot().GetDescendants().Cast<Document>().Where(x => x.ContentType.Alias == "uBlogsyFolderMonth");
            foreach (var doc in nodes)
            {
                doc.delete();
            }

            nodes = TestHelper.GetRoot().GetDescendants().Cast<Document>().Where(x => x.ContentType.Alias == "uBlogsyFolderYear");
            foreach (var doc in nodes)
            {
                doc.delete();
            }

            umbraco.library.RefreshContent();


        }
    }
}
